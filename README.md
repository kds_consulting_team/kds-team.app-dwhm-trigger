# DWHM trigger app

Due to the current design of Keboola Snowflake writer and limitations coming from the security precautions tied with the usage of Keboola provided Snowflake DWH to manage custom schemas and users,
every new table or a table loaded using the Full Load is not visible to existing R/O, R/W roles on given schema until the role is refreshed.

This Keboola component allows  refreshing particular schemas from within any project, remotely using an existing DWHM configuration. This helps refreshing only schemas that were updated and only at times needed.

## Setup

To create a new configuration enter URL

https://connection.eu-central-1.keboola.com/admin/projects/`[PROJECT_ID]`/applications/kds-team.app-dwhm-trigger

 and replace the `[PROJECT_ID]` with ID of the project you want the configuration in.

This app should be added behind any Snowflake writer and set up to update Schemas used by that writer. This will ensure the data is available instantly after the load, and limits the number of refreshes caused by a scheduled regular job.

Now create a new configuration and fill in required parameters:

- **KBC Storage API token** - Storage token of the project where DWHM configuration is located
- **KBC Stack** - Region the project is present (EU)
- **DWHM Config ID**  - ID of the DWHM configuration (the last part of the URL when in config detail, e.g. https://connection.eu-central-1.keboola.com/admin/projects/PROJECT_ID/applications/keboola.app-snowflake-dwh-manager/`4121312`
- **Schemas** - List of schemas to update. The name must match exactly the schema created by the DWHM app in Snowflake, or as it is defined in the DWHM app configuration. If any of schemas is not found, the application fails.

![config](docs/cfg.png)



***NOTE:** When running Snowflake configuration row manually don't forget to trigger this application manually as well.

****NOTE:** In a near future the Snowflake writer will support full load without a need to do the refresh. The refresh is still going to be needed after a new table creation.



## Development
 
This example contains runnable container with simple unittest. For local testing it is useful to include `data` folder in the root
and use docker-compose commands to run the container or execute tests. 

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to your custom path:
```yaml
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
```

Clone this repository, init the workspace and run the component with following command:

```
git clone https://bitbucket.org:kds_consulting_team/kbc-python-template.git my-new-component
cd my-new-component
docker-compose build
docker-compose run --rm dev
```

Run the test suite and lint check using this command:

```
docker-compose run --rm test
```

# Integration

For information about deployment and integration with KBC, please refer to the [deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/) 