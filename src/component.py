'''
Template Component main class.

'''

import json
import logging
import sys
import time

import requests
from kbc.env_handler import KBCEnvHandler
from kbcstorage.base import Endpoint

# configuration variables
SNOWFLAKE_DWH_MANAGER_ID = 'keboola.app-snowflake-dwh-manager'
KEY_SCHEMAS = 'schemas'
KEY_REGION = 'region'
KEY_CONFIG_ID = 'config_id'
KEY_TOKEN = '#token'

MANDATORY_PARS = [KEY_SCHEMAS, KEY_REGION, KEY_TOKEN]
MANDATORY_IMAGE_PARS = []

APP_VERSION = '0.0.1'

REGIONS = {'us-east-1': 'https://connection.keboola.com',
           'eu-central-1': 'https://connection.eu-central-1.keboola.com'}


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS)
        # override debug from config
        if self.cfg_params.get('debug'):
            debug = True

        self.set_default_logger('DEBUG' if debug else 'INFO')
        if not debug:
            sys.tracebacklimit = 0
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validate_config()
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            logging.error(e)
            exit(1)

    def run(self):
        '''
        Main execution code
        '''
        params = self.cfg_params  # noqa

        token = params.get(KEY_TOKEN)
        region = params.get(KEY_REGION)
        config_id = params.get(KEY_CONFIG_ID)
        schemas = params.get(KEY_SCHEMAS)

        logging.info("Collecting DWHM config detail ID: %s", config_id)
        schema_rows = self.get_schemas(token, region, config_id)

        self.validate_schemas(schema_rows, schemas)

        for schema in schemas:
            self.trigger_refres(schema_rows[schema], config_id, token, region)

        logging.info("All jobs finished")

    def validate_schemas(self, schema_rows, schemas):
        missing_schemas = []
        for s in schemas:
            if not schema_rows.get(s):
                missing_schemas.append(s)

        if missing_schemas:
            raise ValueError(
                F'Some schemas are not defined in the DWHM configuration, please check the names. {missing_schemas}')

    def get_schemas(self, token, region, config_id):

        if REGIONS.get(region):
            url = REGIONS[region]
        else:
            raise ValueError('Unsupported region {}'.format(region))

        sourceEndpoint = Endpoint(url, 'components', token)

        cfg = sourceEndpoint._get(
            sourceEndpoint.base_url + '/' + SNOWFLAKE_DWH_MANAGER_ID + '/configs/' + str(config_id))
        rows = cfg['rows']

        schemas = {}
        for r in rows:
            schema = r.get('configuration', {}).get('parameters', {}).get('business_schema')
            if schema:
                schemas[schema['schema_name']] = r['id']

        return schemas

    def run_config(self, config_id, token, region, row_id):
        values = {
            "config": config_id,
            "row": row_id
        }

        headers = {
            'Content-Type': 'application/json',
            'X-StorageApi-Token': token
        }
        if region == 'us-east-1':
            base_url = 'https://syrup.keboola.com/docker/'
        else:
            base_url = 'https://syrup.' + region + '.keboola.com/docker/'
        response = requests.post(base_url + SNOWFLAKE_DWH_MANAGER_ID + '/run', data=json.dumps(values),
                                 headers=headers)

        try:
            response.raise_for_status()
        except requests.HTTPError as e:
            raise e
        else:
            return response.json()

    def get_job_status(self, token, url):
        headers = {
            'Content-Type': 'application/json',
            'X-StorageApi-Token': token
        }
        response = requests.get(url, headers=headers)
        try:
            response.raise_for_status()
        except requests.HTTPError as e:
            raise e
        else:
            return response.json()

    def trigger_refres(self, row_id, config_id, token, region):
        res = self.run_config(config_id, token, region, row_id)
        poll_url = res['url']
        status = self.get_job_status(token, poll_url)
        is_timedout = False
        start = time.time()
        while status['status'] in ['waiting', 'processing'] and not is_timedout:
            status = self.get_job_status(token, poll_url)
            end = time.time()
            if (end - start) > 3600:
                is_timedout = True
        return status


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug = sys.argv[1]
    else:
        debug = False
    comp = Component(debug)
    try:
        comp.run()
    except Exception as e:
        logging.error(e)
